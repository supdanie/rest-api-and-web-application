import base64
import json
import requests
from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from rest_framework import status
from api.forms import VideoForm, ProcessForm, LoginForm
from api.video_processing_rest_api.storage import RedisStorage
from api.video_processing_rest_api.models import get_token_from_username_and_password
from django.contrib import messages

def login_view(request, *args, **kwargs):
    """
    This function processes a HTTP request for getting the login page or HTTP POST request sent after submitting the login form.

    :param request: The HTTP request to process.
    :param args:
    :param kwargs:
    :return: Rendered template for login page
    """
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)
        username = form.data['username']
        password = form.data['password']
        if authenticate(username=username, password=password) is not None:
            request.session['token'] = get_token_from_username_and_password(username, password)
            return redirect(request.session['redirectURL'])
        else:
            form.add_error(None, 'Invalid credentials')
    return render(request, 'login.html', {'form': form})

def home_view(request, *args, **kwargs):
    """
    This function processes a HTTP request for getting the homepage with the list of uploaded videos and form for uploading a video to the server. It checks whether the user is logged in. If the user is not logged in, it redirects the user to login page.

    :param request: HTTP request to process.
    :param args:
    :param kwargs:
    :return: Rendered template for homepage or redirect to login page if the user is not logged in. It also processes the form for uploading a video and shows a sucess message if the video has a valid extension.
    """
    if 'token' not in request.session:
        request.session['redirectURL'] = '/'
        return redirect('/login')

    form = VideoForm()
    if request.method == 'POST':
        form = VideoForm(request.POST, request.FILES)
        if form.is_valid():
            messages.success(request, 'Video successfully uploaded.')
    return render(request, "home.html", {"page_title": "Můj titulek", 'form': form, 'token': request.session['token']})

def processes_view(request, pk=None, *args, **kwargs):
    """
    This function checks whether the user is logged in. If the user is not logged in, it redirects him to the login page. Then it checks whether a video with the given identificator exists. If the video with the given identificator doesn't exist, it returns a response with status code 404 - Not Found. Otherwise it renders a template with the list of processes for the given video.

    :param request: HTTP request to process.
    :param pk: Identificator of the video.
    :param args:
    :param kwargs:
    :return: If the user is not logged in, it returns a redirect to login page. If the video with the given identificator is not found, it returns a HTTPNotFoundResponse. Otherwise it returns a rendered template with the list of processes.
    """
    if 'token' not in request.session:
        request.session['redirectURL'] = '/video/' + str(pk) + '/process'
        return redirect('/login')

    storage = RedisStorage()
    video = storage.get_video_info(pk)
    if video is None:
        return HttpResponseNotFound("Video not found.")

    return render(request, 'processes.html', {'video_id': pk, 'video_path': video['path'], 'token': request.session['token']})

def add_process_view(request, pk=None, *args, **kwargs):
    """
    This function checks whether the user is logged in. If the user is not logged in, it redirects the user to the login page. Then it checks whether the video with the given identificator exists. If the video with the given identificator doesn't exists, it returns a HTTPNotFoundResponse. Otherwise it processes the form for starting a process and ensures showing a sucess message if values in form are valid. Then it renders a template with the form for starting a new process.

    :param request: HTTP request to process.
    :param pk: Identificator of the video.
    :param args:
    :param kwargs:
    :return: If the user is not logged in, it returns a redirect to the login page. If the video with the given identificator doesn't exist, it returns a HTTPNotFoundResponse. Otherwise it returns a rendered template of the page with the form for starting a new process.
    """
    if 'token' not in request.session:
        request.session['redirectURL'] = '/video/' + str(pk) + '/add_process'
        return redirect('/login')

    storage = RedisStorage()
    if storage.get_video_info(pk) is None:
        return HttpResponseNotFound('Video not found.')

    form = ProcessForm()
    if request.method == 'POST':
        form = ProcessForm(request.POST)
        if form.is_valid():
            messages.success(request, 'Process successfully sent for processing.')
            return redirect('/video/' + str(pk) + '/process')

    return render(request, 'add_process.html', {'form': form, 'video_id': pk, 'token': request.session['token']})

def process_detail_view(request, video_pk=None, pk=None, *args, **kwargs):
    """
    It checks whether the user is logged in. If the user is not logged in, it redirects him to the login page. Then it checks whether a video with the given identificator and a process with the given identificator exist. If the video with the given identificator or the process with the given identificator doesn't exist, it returns a response with status code 404. Otherwise it returns a rendered template with the process detail with video and parameters of the process.

    :param request: HTTP request to process
    :param video_pk: Identificator of the video
    :param pk: Identificator of the process
    :param args:
    :param kwargs:
    :return: If the user is not logged in, it returns a redirect to the login page. If a process with the given identificator of a video with the given identificator doesn't exist, it returns a HTTPNotFoundResponse. Otherwise it returns a rendered template with the detail of the process with parameters and video.
    """
    if 'token' not in request.session:
        request.session['redirectURL'] = '/video/' + str(video_pk) + '/process/' + str(pk)
        return redirect('/login')

    storage = RedisStorage()
    if storage.get_process_info(video_pk, pk) is None:
        return HttpResponseNotFound('Process not found.')

    video_path = storage.get_video_info(video_pk)['path']
    return render(request, 'process_detail.html', {'video_path': video_path, 'video_id': video_pk, 'process_id': pk, 'token': request.session['token']})