import base64
import json
import os
import time
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth.models import User
from django.test import Client
from api.video_processing_rest_api.storage import RedisStorage

class VideoProcessingTestCase(APITestCase):

    def setUp(self) -> None:
        user = User.objects.create(username='admin')
        user.set_password('Bubovice')
        user.save()

        storage = RedisStorage()
        videos = storage.get_videos()
        self.__existing_videos = []
        for video in videos:
            video_id = video['id']
            self.__existing_videos.append(video_id)

    def tearDown(self) -> None:
        storage = RedisStorage()
        videos = storage.get_videos()
        for video in videos:
            video_id = video['id']
            if video_id not in self.__existing_videos:
                storage.delete_video(video_id)

    def test_get_videos_with_none_or_bad_authorization(self):
        response = self.client.get('/api/video/', content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.get('/api/video/', content_type='application/json',
                                   HTTP_AUTHORIZATION='BaseToken YWRta345CCC')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_good_complex_scenery(self):
        # get list of videos
        response = self.client.get('/api/video/', content_type='application/json', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        videos = len(json.loads(response.content))

        # loading a new video
        current_file_path = os.path.dirname(os.path.realpath(__file__))
        video_path = os.path.join(current_file_path, 'video/video1.mp4')
        video_file = open(video_path, "rb")
        video_content = video_file.read()
        video_file.close()
        video = SimpleUploadedFile(video_path, video_content, content_type="video/mp4")
        response = self.client.post('/api/video/', {'video': video}, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('Location', response)
        video_location = response['Location']
        video_id = video_location.split("/")[-1]

        # get list of videos after loading the new video
        response = self.client.get('/api/video/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        videos_new = len(json.loads(response.content))
        self.assertEqual(videos_new - videos, 1)

        # starting a new process
        params = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        response = self.client.post('/api/video/' + video_id + '/process/', params, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertIn('Location', response)
        process_location = response['Location']
        process_id = process_location.split('/')[-1]

        time.sleep(1)

        response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        time.sleep(30)

        # getting the list of the processes
        response = self.client.get('/api/video/' + video_id + '/process/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        processes = json.loads(response.content)
        process_id = process_location.split('/')[-1]

        # getting the detail of the process
        response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/',  HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertNotEqual(content, [])
        self.assertNotEqual(content, {})

        # getting the list of objects
        response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/object/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        for object in content:
            object_id = object.split('/')[-1]
            response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/object/' + object_id + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            invalid_formats = ['video/webm', 'image/jpeg', 'image/*', 'video/*', 'text/javascript', 'text/uri-list']
            for invalid_format in invalid_formats:
                response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/object/' + object_id + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
                self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

        response = self.client.delete('/api/video/' + video_id + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_getting_video_with_bad_id(self):
        storage = RedisStorage()
        bad_id = storage.get_video_max_id() + 2
        response = self.client.get('/api/video/' + str(bad_id) + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_video_with_bad_id(self):
        storage = RedisStorage()
        bad_id = storage.get_video_max_id() + 2
        response = self.client.delete('/api/video/' + str(bad_id) + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_getting_process_with_bad_id(self):
        video_max_id = 0
        for i in range(2):
            # loading a new video
            current_file_path = os.path.dirname(os.path.realpath(__file__))
            video_path = os.path.join(current_file_path, 'video/video1.mp4')
            video_file = open(video_path, "rb")
            video_content = video_file.read()
            video_file.close()
            video = SimpleUploadedFile(video_path, video_content, content_type="video/mp4")
            response = self.client.post('/api/video/', {'video': video}, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertIn('Location', response)
            video_max_id = int(response['Location'].split('/')[-1])

        process_ids = []
        for video_id in [video_max_id - 1, video_max_id]:
            # starting a new process
            params = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480,
                      'width': 1280, 'height': 960}
            response = self.client.post('/api/video/' + str(video_id) + '/process/', params, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
            self.assertIn('Location', response)
            process_ids.append(int(response['Location'].split('/')[-1]))
            time.sleep(30)

        first_scenery = [video_max_id - 1, process_ids[0], status.HTTP_200_OK]
        second_scenery = [video_max_id, process_ids[1], status.HTTP_200_OK]
        third_scenery = [video_max_id - 1, process_ids[1], status.HTTP_404_NOT_FOUND]
        for scenery in [first_scenery, second_scenery, third_scenery]:
            video_id = scenery[0]
            process_id = scenery[1]
            response_status = scenery[2]
            response = self.client.get('/api/video/' + str(video_id) + '/process/' + str(process_id) + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, response_status)
            response = self.client.get('/api/video/' + str(video_id) + '/process/' + str(process_id) + '/object/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, response_status)

    def test_processes_with_invalid_parameters(self):
        # loading a new video
        current_file_path = os.path.dirname(os.path.realpath(__file__))
        video_path = os.path.join(current_file_path, 'video/video1.mp4')
        video_file = open(video_path, "rb")
        video_content = video_file.read()
        video_file.close()
        video = SimpleUploadedFile(video_path, video_content, content_type="video/mp4")
        response = self.client.post('/api/video/', {'video': video}, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('Location', response)
        video_id = response['Location'].split('/')[-1]

        params1 = {'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}
        params2 = {'h': 15000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'width': 1280, 'height': 960}
        params3 = {'h': 15000, 'alpha': 45, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}
        params4 = {'h': 15000, 'alpha': 45, 'f': 10, 'len_width': 8, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}
        params5 = {'h': 150000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'further_param': 'value'}

        for params in [params1, params2, params3, params4, params5]:
            response = self.client.post('/api/video/' + video_id + '/process/', params, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_processes_with_bad_values_of_params(self):
        # loading a new video
        current_file_path = os.path.dirname(os.path.realpath(__file__))
        video_path = os.path.join(current_file_path, 'video/video1.mp4')
        video_file = open(video_path, "rb")
        video_content = video_file.read()
        video_file.close()
        video = SimpleUploadedFile(video_path, video_content, content_type="video/mp4")
        response = self.client.post('/api/video/', {'video': video}, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('Location', response)
        video_id = response['Location'].split('/')[-1]

        params1 = {'h': -1, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params2 = {'h': 13000, 'alpha': 45, 'f': 0, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params3 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': -6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960}
        params4 = {'h': 13000, 'alpha': 45, 'f': '10 mm', 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.000000000000001]}
        params5 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 0, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params6 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': -640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params7 = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': 'string', 'p': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params8 = {'h': 13000, 'alpha': '45 degrees', 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params9 = {'h': 13000, 'alpha': '45 degrees', 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': '1280 px', 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        params10 = {'h': 13000, 'alpha': '45', 'f': 10, 'len_width': '8 mm', 'len_height': 6, 'p_x': 640, 'p_y': 480, 'width': 1280, 'height': 960, 'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        for params in [params1, params2, params3, params4, params5, params6, params7, params8, params9, params10]:
            response = self.client.post('/api/video/' + video_id + '/process/', params, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_getting_list_of_videos_invalid_formats(self):
        invalid_formats = ['video/mp4', 'image/jpeg', 'video/*', 'audio/*', 'audio/wav', 'application/pdf', 'text/csv', 'text/javascript']
        for invalid_format in invalid_formats:
            response = self.client.get('/api/video/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

    def test_getting_video_detail_invalid_formats(self):
        current_file_path = os.path.dirname(os.path.realpath(__file__))
        video_path = os.path.join(current_file_path, 'video/video1.mp4')
        video_file = open(video_path, "rb")
        video_content = video_file.read()
        video_file.close()
        video = SimpleUploadedFile(video_path, video_content, content_type="video/mp4")
        response = self.client.post('/api/video/', {'video': video}, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('Location', response)
        video_location = response['Location']
        video_id = video_location.split("/")[-1]

        invalid_formats = ['video/mp4', 'image/jpeg', 'video/*', 'audio/*', 'audio/wav', 'application/pdf', 'text/*', 'text/csv', 'text/javascript', 'text/uri-list']
        for invalid_format in invalid_formats:
            response = self.client.get('/api/video/' + video_id + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

            response = self.client.get('/api/video/' + str(int(video_id) + 15) + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_getting_list_processes_and_objects_invalid_formats(self):
        current_file_path = os.path.dirname(os.path.realpath(__file__))
        video_path = os.path.join(current_file_path, 'video/video1.mp4')
        video_file = open(video_path, "rb")
        video_content = video_file.read()
        video_file.close()
        video = SimpleUploadedFile(video_path, video_content, content_type="video/mp4")
        response = self.client.post('/api/video/', {'video': video}, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('Location', response)
        video_location = response['Location']
        video_id = video_location.split('/')[-1]



        # starting a new process
        params = {'h': 13000, 'alpha': 45, 'f': 10, 'len_width': 8, 'len_height': 6, 'p_x': 640, 'p_y': 480,
                  'width': 1280, 'height': 960,
                  'k': [0.0000000000000004, 0.000000000000000005, 0.0000000000000000000004]}
        response = self.client.post('/api/video/' + video_id + '/process/', params, HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertIn('Location', response)
        process_id = response['Location'].split('/')[-1]

        time.sleep(30)

        invalid_formats_list = ['video/mp4', 'image/jpeg', 'video/*', 'audio/*', 'audio/wav', 'application/pdf', 'text/csv',
                           'text/javascript']
        invalid_formats_detail = ['video/mp4', 'image/jpeg', 'video/*', 'audio/*', 'audio/wav', 'application/pdf', 'text/*', 'text/csv', 'text/javascript', 'text/uri-list']
        for invalid_format in invalid_formats_list:
            response = self.client.get('/api/video/' + video_id + '/process/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

            response = self.client.get('/api/video/' + str(int(video_id) + 15) + '/process/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

            response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/object/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

            response = self.client.get('/api/video/' + video_id + '/process/' + str(int(process_id) + 15) + '/object/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        for invalid_format in invalid_formats_detail:
            response = self.client.get('/api/video/' + video_id + '/process/' + process_id + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)

            response = self.client.get('/api/video/' + video_id + '/process/' + str(int(process_id) + 15) + '/', HTTP_AUTHORIZATION='Basic YWRtaW46QnVib3ZpY2U=', **{'Accept': invalid_format})
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)