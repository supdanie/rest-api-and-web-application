from rest_framework import viewsets, status
from rest_framework.response import Response
from django.http.request import QueryDict, MultiValueDict
from django_q.tasks import async_task, result
import exiftool
from video_processing_library.video_processor import VideoProcessor
from video_processing_library.object_detector import BackgroundSubtractionObjectDetector
from video_processing_library.annotation import MediaFragmentAndWebVTTAnnotation
from video_processing_library.speed_measurement import CountingPointDistanceSpeedMeasurement
import api.constants as constants
import api.constants_objects as constants_objects
import api.settings as settings
import os


class BaseViewSet(viewsets.ViewSet):
	"""
	This class is a parent class for all view sets in the application.
	"""

	def _video_and_process_exist(self, video_pk=None, pk=None):
		"""
		This method checks whether the video with the given identificator exists and process with the given identificator exists and whether the process was ran for the video with the given identificator.

		:param video_pk: The identificator of the video.
		:param pk: The identificator of the process.
		:return: Boolean representing whether the process and the video exist and whether the process was ran for the video with the given identificator.
		"""
		if constants_objects.STORAGE.get_video_info(video_pk) is None:
			return False
		if constants_objects.STORAGE.get_process_info(video_pk, pk) is None:
			return False
		return True

	def _invalid_format(self, request, valid_formats):
		accept_header = request.META.get('Accept')
		if accept_header is not None and accept_header not in valid_formats:
			return True
		return False

	def _invalid_format_list(self, request):
		valid_formats = ['*/*', 'application/*', 'application/json']
		return self._invalid_format(request, valid_formats)

	def _invalid_format_detail(self, request):
		valid_formats = ['*/*', 'application/*', 'application/json']
		return self._invalid_format(request, valid_formats)

	def _none_or_invalid_token(self, request):
		authorization_header = request.headers.get('Authorization')
		if authorization_header is None:
			return True

		return 'Basic ' not in authorization_header


class VideoViewSet(BaseViewSet):
	"""
	It is a class for processing any HTTP request for making an operation with video such as adding, removing, getting a detail and getting a list of all video.
	"""
	def list(self, request):
		"""
		It gets the list of URIs to the video which are uploaded to the server.

		:param request: Representation of the sent HTTP request for processing.
		:return: Response with a list of URIs to video which are uploaded to the server.
		"""
		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if self._invalid_format_list(request):
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

		return Response(constants_objects.STORAGE.get_videos())

	def retrieve(self, request, pk=None):
		"""
		It gets the detail of the video which contains metaadata and list of processed ran for this video.

		:param request: Representation of the sent HTTP request for processing.
		:param pk: Unique identificator of the video
		:return: Response with detail of the video with metadata and list of processes which processed or are processing the video.
		"""
		if constants_objects.STORAGE.get_video_info(pk) is None:
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if self._invalid_format_detail(request):
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

		return Response(constants_objects.STORAGE.get_video_info(pk))

	def destroy(self, request, pk=None):
		"""
		It removes the video with the given identification with all detailed information and processes ran for this video.

		:param request: Representation of the sent HTTP request for processing.
		:param pk: The identification of the video which has to be removed with all processes and detected objects.
		:return: Response with according status (204 - No content if the video exists, 404 - Not found - if the video does not exist)
		"""
		if constants_objects.STORAGE.get_video_info(pk) is None:
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		constants_objects.STORAGE.delete_video(pk)
		return Response(status=status.HTTP_204_NO_CONTENT)

	def create(self, request):
		"""
		It uploads the video to the server and saves metadata for the video.

		:param request: Representation of the sent HTTP request for processing.
		:return: Response with Location header with the link to the newly uploaded video with metadata and status 201 - Created.
		"""
		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if 'video' not in request.data:
			return Response(status=status.HTTP_400_BAD_REQUEST)
		video_id = constants_objects.STORAGE.get_video_max_id() + 1
		file_extension = str(request.data['video']).split('.')[-1]
		video_file = 'video' + str(video_id) + '.' + file_extension
		if file_extension not in ['mp4', 'ogv', 'webm']:
			return Response(status=status.HTTP_400_BAD_REQUEST)
		video_path = os.path.join(settings.MEDIA_ROOT, video_file)
		self.__save_video(video_path, request.data)
		video_metadata = self.__get_video_metadata(video_path)
		authorization_token = request.headers.get('Authorization').replace('BaseToken ', '')
		constants_objects.STORAGE.save_video_with_metadata(video_id, authorization_token, video_file, video_metadata)
		response = Response(status=status.HTTP_201_CREATED)
		response['Location'] = constants.API_URL + 'video/' + str(video_id)
		return response

	def __save_video(self, video_path, request_data):
		"""
		This additional method saves the video from the request to the video file with the given path.

		:param video_path: Relative or absolute path to the video file where the video from the request has to be saved
		:param request_data: The data sent in the request
		"""
		video = request_data["video"]
		output = open(video_path, "wb")
		content = video.read()
		output.write(content)
		video.close()
		output.close()

	def __get_video_metadata(self, video_path):
		"""
		This method extracts metadata from the video with the given path with exiftool.

		:param video_path: Relative or absolute path to the video
		:return: Hash with metadata of the given video (type, MIME typpe, duration, frame rate and dimensions of the frames in pixels)
		"""
		with exiftool.ExifTool() as et:
			metadata = et.get_metadata_batch([video_path])

		type = metadata[0]['File:FileType']
		mime_type = metadata[0]['File:MIMEType']
		duration_key = 'Composite:Duration'
		video_frame_rate_key = 'QuickTime:VideoFrameRate'
		image_size_key = 'Composite:ImageSize'
		for key in metadata[0]:
			if 'Duration' in key:
				duration_key = key
			elif 'VideoFrameRate' in key or 'FrameRate' in key:
				video_frame_rate_key = key
			elif 'ImageSize' in key:
				image_size_key = key
		duration = metadata[0][duration_key]
		frame_rate = metadata[0][video_frame_rate_key]
		image_size = metadata[0][image_size_key].split('x', 2)
		width = image_size[0]
		height = image_size[1]
		metadata_hash = {'type': type, 'mimeType': mime_type, 'duration': duration, 'frameRate': frame_rate, 'width': width, 'height': height}
		return metadata_hash


def process_video_and_generate_annotations(video_path, data, video_pk, process_id):
	"""
	This function ensures processing the video and saving generated annotations to the given storage. It is called asynchronously after sending a HTTP request for starting a new process of processing video.

	:param video_path: Relative or absolute path to the video
	:param data: Data from the sent HTTP request
	:param video_pk: Identificator of the processed video
	:param process_id: Identificator of the running process
	:param storage_class: Class which is used for saving data about the running process and generated objects during the process
	"""
	storage = constants_objects.STORAGE
	storage.set_process_status(video_pk, process_id, constants.STATUS_RUNNING)
	converted_params_with_url(data)
	params = converted_params_with_url(data)
	annotation_generator = VideoProcessor().process_video(video_path, params,
														  BackgroundSubtractionObjectDetector(),
														  CountingPointDistanceSpeedMeasurement,
														  MediaFragmentAndWebVTTAnnotation())
	for annotations in annotation_generator:
		if (storage.get_process_status(video_pk, process_id) != constants.STATUS_RUNNING):
			break
		storage.save_detected_objects_with_annotations(video_pk, process_id, annotations)

	if (storage.get_process_status(video_pk, process_id) != constants.STATUS_RUNNING):
		storage.delete_process(video_pk, process_id)
	else:
		storage.set_process_status(video_pk, process_id, constants.STATUS_DONE)

def converted_params_with_url(data):
	"""
	This function converts dictionary (probably QueryDict) from the HTTP request to process with parameters without the server URL to a hash with the server URL. It also converts arrays with one element (if the parameter key is not k or p) to a scalar value.

	:param data: The dictionary (probably QueryDict object) from the HTTP request to process
	:return: Dictionary with the server URL and scalar values instead of arrays with one element and params from the HTTP request
	"""
	params = {'server_url': constants.MEDIA_URL}
	dictionary = data.lists() if isinstance(data, QueryDict) else data
	for param, value in dictionary:
		array_value = convert_to_correct_literals(value)
		if len(array_value) == 1 and param not in ['k', 'p']:
			params[param] = array_value[0]
		else:
			params[param] = array_value
	return params

def convert_to_correct_literals(array_from_params):
	"""
	This function converts elements in array from strings to floats.

	:param array_from_params: Array with strings which have to be converted to float.
	:return: Array with float instead of strings which can be converted to float.
	"""
	if isinstance(array_from_params, list):
		list_with_elements = []
		for element in array_from_params:
			list_with_elements.append(convert_to_correct_literals(element))
		return list_with_elements
	else:
		try:
			return float(array_from_params)
		except ValueError:
			return array_from_params

class ProcessViewSet(BaseViewSet):
	"""
	It ensures processing HTTP request for making an operation with processes such as starting a new process for processing a video, getting detail of the given process, removing a process or getting a list of processes ran for the video with the given identificator.
	"""
	def list(self, request, video_pk=None):
		"""
		It gets the list of all processed ran for the video with the given identificator.

		:param request: Representation of the sent HTTP request for processing.
		:param video_pk: Unique identificator of the video
		:return: If the video with the given identificator exists, it returns a list of processes ran for the video. Otherwise it returns a response with status code 404 - Not Found.
		"""
		if constants_objects.STORAGE.get_video_info(video_pk) is None:
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if self._invalid_format_list(request):
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

		return Response(constants_objects.STORAGE.get_processes(video_pk))

	def retrieve(self, request, video_pk=None, pk=None):
		"""
		It gets the detailed information about the given process.

		:param request: Representation of the sent HTTP request for processing.
		:param video_pk: Unique identificator of the video
		:param pk: Identificator of the process
		:return: If the process exists and it was ran for the video with the given identificator, it returns a response with detailed information about the given process. Otherwise it returns a response with status code 404 - Not Found.
		"""
		if not self._video_and_process_exist(video_pk, pk):
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if self._invalid_format_detail(request):
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

		if constants_objects.STORAGE.get_process_status(video_pk, pk) != constants.STATUS_DONE:
			return Response(status=status.HTTP_202_ACCEPTED)

		return Response(constants_objects.STORAGE.get_process_info(video_pk, pk))

	def create(self, request, video_pk=None):
		"""
		It starts a new process for processing the video, object detection and speed measurement asynchronously.

		:param request: Representation of the sent HTTP request for processing.
		:param video_pk: Unique identificator of the video
		:return: If the video with the given identificator exists and parameters in the request are valid, it returns a response with status code 202 - Accepted and Location header to the new started process. If the given parameters in the request body are invalid, it returns a HTTP response with status code 400. If the video with the given identificator does not exist, it returns a response with status code 404.
		"""
		if constants_objects.STORAGE.get_video_info(video_pk) is None:
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if not self.__validate_parameters(request.data):
			return Response(status=status.HTTP_400_BAD_REQUEST)

		video_path = constants_objects.STORAGE.get_video_info(video_pk)['path']
		video_path = video_path.replace(constants.MEDIA_URL, '')
		video_path = os.path.join(settings.MEDIA_ROOT, video_path)
		process_id = constants_objects.STORAGE.get_process_max_id() + 1
		constants_objects.STORAGE.save_process_with_info(video_pk, process_id, request.data, "metadata" + str(process_id) + ".vtt")

		async_task(process_video_and_generate_annotations, video_path, request.data, video_pk, process_id)

		response = Response(status=status.HTTP_202_ACCEPTED)
		response['Location'] = constants.API_URL + 'video/' + str(video_pk) + '/process/' + str(process_id)
		return response

	def destroy(self, request, video_pk=None, pk=None):
		"""
		It deletes the given process and detected object during the process.

		:param request: Representation of the sent HTTP request for processing.
		:param video_pk: Identificator of the video
		:param pk: Identificator of the process to delete
		:return: If the process with the given identificator exists and it processes the video with the given identificator, it returns a response with status code 204 - No Content. Otherwise it returns a response with status code 404 - Not Found.
		"""
		if not self._video_and_process_exist(video_pk, pk):
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		constants_objects.STORAGE.delete_process(video_pk, pk)
		return Response(status=status.HTTP_204_NO_CONTENT)

	def __validate_parameters(self, request_body):
		"""
		This method validates parameters for starting a new process. It checks whether the body of the request contains parameters important for speed measurement of detected objects during the new process and whether contains only required parameters and allowed parameters (distortion coefficients, distortion centre and yaw angle).

		:param request_body: Body of the HTTP request for processing.
		:return: Boolean whether the body contains parameteres required for the speed measurement and whether it contains only required parameters and other allowed parameters (distortion coefficients, distortion centre and yaw angle).
		"""
		params = converted_params_with_url(request_body)
		return CountingPointDistanceSpeedMeasurement.validate_params(params)

class ObjectViewSet(BaseViewSet):
	"""
	This class processes a HTTP request for getting information about detected moving objects.
	"""
	def list(self, request, video_pk=None, process_pk=None):
		"""
		It gets the list of moving objects detected during the process.

		:param request: Representation of the sent HTTP request for processing.
		:param video_pk: Unique identificator of the video
		:param process_pk: Identificator of the process
		:return: If the process exists and processed the video with the given identificator, it returns a response with the list of objects detected during the process. Otherwise it returns a response with status code 404 - Not Found.
		"""
		if not self._video_and_process_exist(video_pk, process_pk):
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if self._invalid_format_list(request):
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

		objects = constants_objects.STORAGE.get_objects(video_pk, process_pk)
		objects_response_body = []
		for object in objects:
			object_id = int(object)
			object_url = constants.API_URL + 'video' + str(video_pk) + '/process/' + str(process_pk) + '/object/' + str(object_id)
			objects_response_body.append(object_url)
		return Response(objects_response_body)

	def retrieve(self, request, video_pk=None, process_pk=None, pk=None):
		"""
		It gets the detail of the moving object with annotations with information about position, size in time and speeds.

		:param request: Representation of the sent HTTP request for processing.
		:param video_pk: Unique identificator of the video
		:param process_pk: Identificator of the process
		:param pk: Identificator of the moving object detected during the given process
		:return: If the object with the given identificator exists and it was detected during the given process, it returns a response with annotations of the object. Otherwise it returns a response with status code 404 - Not Found.
		"""
		if not self._video_and_process_exist(video_pk, process_pk):
			return Response(status=status.HTTP_404_NOT_FOUND)

		if self._none_or_invalid_token(request):
			return Response(status=status.HTTP_401_UNAUTHORIZED)

		if self._invalid_format_detail(request):
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

		return Response(constants_objects.STORAGE.get_object_annotations(video_pk, process_pk, pk))