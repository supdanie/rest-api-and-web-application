from django_redis import get_redis_connection
import os
import api.constants as constants
import api.settings as settings
import base64
from api.video_processing_rest_api.models import get_token_from_username_and_password

class Storage:
    """
    This class represents a storage which can be used for saving information about video, processes and detected objects. It is a parent class of all classes which represent a concrete type of storage or database such as Redis.
    """
    def save_video_with_metadata(self, video_id, author, path, metadata):
        """
        This method saves the uploaded video with the given path and got metadata. This implementation is empty, because it is an abstract method.

        :param video_id: Identificator of the video
        :param path: Relative or absolute path to the video
        :param metadata: Metadata of the video with duration, frame dimensions, type and MIME type
        """
        pass

    def save_process_with_info(self, video_id, process_id, params, webVTT_path):
        """
        This method saves the ran process with the information including path to WebVTT file with metadata and given parameters for algorithm for speed measurement of objects. This implementation is empty, because it is an abstract method.

        :param video_id: Identificator of the video.
        :param process_id: Identificator of the process.
        :param params: Map with parameters for starting the process which are used for the algorithm for counting speed of the detected objects
        :param webVTT_path: Relative or absolute path to WebVTT file with metadata including measured speeds of the objects detected during the process
        """
        pass

    def save_detected_objects_with_annotations(self, video_id, process_id, annotations):
        """
        This method saved detected objects and the given annotations of the detected objects. This implementation is empty, because it is an abstract method.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :param annotations: Generated annotations with information about the detected objects such as time, position, size and counted speed
        """
        pass

    def get_videos(self):
        """
        This method gets and returns the list of uploaded video. This implementation returns None, because it is an abstract method.

        :return: None, because it is a dummy implementation
        """
        return None

    def get_video_info(self, video_id):
        """
        This method gets and returns the list of detailed information about the video with the given identificator such as metadata and path of the video. This implementation returns None, because it is an abstract method.

        :param video_id: Identificator of the video
        :return: None, because it is a dummy implementation
        """
        return None

    def get_processes(self, video_id):
        """
        This method gets and returns the list of ran processes for the video with the given identificator. This implementation returns None, because it is an abstract method.

        :param video_id: Identificator of the video
        :return: None, because it is a dummy implementation
        """
        return None

    def get_process_info(self, video_id, process_id):
        """
        This method gets and returns the detailed information about the process with the given identificator which was ran about the given video. The information include the given parameters for algorithm for speed measurement, path to WebVTT file with metadata and list of detected objects. This implementation returns None, because it is an abstract method.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :return: None, because it is a dummy implementation
        """
        return None

    def get_objects(self, video_id, process_id):
        """
        This method gets and returns a list of detected objects during the process with the given identificator which was ran for the given video. This implementation returns None, because it is an abstract method.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :return: None, because it is a dummy implementation
        """
        return None

    def get_object_annotations(self, video_id, process_id, object_id):
        """
        This method gets and returns annotations for the given detected object. This implementation returns None, because it is an abstract method.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :param object_id: Identificator of the detected moving object
        :return: None, because it is a dummy implementation
        """
        return None

    def save_webVTT_metadata(self, webVTT_file_path, webVTT_fragments):
        """
        This method saves the given metadata to the WebVTT metadata with the given path.

        :param webVTT_file_path: Relative or absolute path to the WebVTT file where the metadata has to be written
        :param webVTT_fragments: List of generated WebVTT cues with counted speed of moving objects
        """
        f = open(webVTT_file_path, 'a')
        for webVTT_fragment in webVTT_fragments:
            f.write(webVTT_fragment + '\n\n')
        f.close()

    def delete_video(self, video_id):
        """
        This method deletes the video and all saved information about the given video, processes ran for this video and detected objects during the ran processes. This implementation is empty, because it is an abstract method.

        :param video_id: Identificator of the video
        """
        pass

    def delete_process(self, video_id, process_id):
        """
        This method deletes the process with all information and detected objects. This implementation is empty, because it is an abstract method.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        """
        pass

    def get_video_max_id(self):
        """
        This method gets and returns the maximal identificator of a saved video from the storage. This implementation returns zero, because it is an abstract method.

        :return: Zero, because it is a dummy implementation
        """
        return 0

    def get_process_max_id(self):
        """
        This method gets and returns the maximal identificator of a ran and saved process from the storage. This implementation returns zero, because it is an abstract method.

        :return: Zero, because it is a dummy implementation
        """
        return 0

    def get_process_status(self, video_id, process_id):
        """
        This method gets and returns the status of the process with the given identificator. There the method returns None, because it is an abstract method.

        :param video_id: Identificator of the video processed by the process
        :param process_id: Identificator of the process
        :return: None, because it is an abstract method.
        """
        return None

    def set_process_status(self, video_id, process_id, status):
        """
        This method sets the status of the process with the given identificator. There the method does nothing, because it is an abstract method.

        :param video_id: Identificator of the video processed by the process
        :param process_id: Identificator of the process
        :param status: Current status of the process to save
        """
        pass

class RedisStorage(Storage):
    """
    This class is responsible for saving information about uploaded video, ran processes and detected objects to key-value database Redis.
    """
    def __init__(self):
        self.__redis_connection = get_redis_connection("default")

    def save_video_with_metadata(self, video_id, author, path, metadata):
        """
        This method saves the video, its path and got metadata to Redis. It also updates the maximal identificator of the uploaded video.

        :param video_id: Identificator of the video
        :param path: Relative or absolute path to the video
        :param metadata: Metadata of the video with duration, frame dimensions, type and MIME type
        """
        self.__redis_connection.set("video:"+str(video_id)+":path", path)
        self.__redis_connection.set("video:" + str(video_id) + ":author", author)
        self.__redis_connection.hmset("video:"+str(video_id)+":metadata", metadata)
        self.__redis_connection.rpush("video", video_id)


    def save_process_with_info(self, video_id, process_id, params, webVTT_path):
        """
        This method saves the process ran for the given video with given parameters and path to WebVTT file where metadata are saved to Redis.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :param params: Map with parameters for starting the process which are used for the algorithm for counting speed of the detected object
        :param webVTT_path: Relative or absolute path to WebVTT file with metadata including measured speeds of the objects detected during the process
        """
        self.__redis_connection.hmset("video:" + str(video_id) + ":process:" + str(process_id) + ":parameters", self.__process_params(params))
        self.__redis_connection.set("video:" + str(video_id) + ":process:" + str(process_id) + ":webVTTFile", webVTT_path)
        self.__redis_connection.sadd("video:" + str(video_id) + ":processes", process_id)
        self.__redis_connection.rpush("process", process_id)
        file_path = os.path.join(settings.ANNOTATIONS_ROOT, webVTT_path)
        self.save_webVTT_metadata(file_path, ['WEBVTT'])

    def __process_params(self, params):
        """
        This method destroys arrays from the parameters and converts it to a keys including index with scalar values.

        :param params: The parameters with arrays given in a HTTP request
        :return: Array with the given parameters with keys created by converting array with one key to key-value pairs where key contains the index from 0 to array size and value is the particular element from the array.
        """
        processed_params = {}
        for param in params:
            if isinstance(params[param], list):
                index = 0
                for element in params[param]:
                    processed_params[param + ":" + str(index)] = element
                    index += 1
            else:
                processed_params[param] = params[param]
        return processed_params

    def save_detected_objects_with_annotations(self, video_id, process_id, annotations):
        """
        This method saves Media Fragments URIs for the detected objects and identificator of the objects to the Redis set of detected objects for the given process.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :param annotations: Array of annotations of the detected objects with time, position and size on the frame and counted speed.
        """
        webVTT_annotations = []
        last_media_fragments_URI = None
        for annotation in annotations:
            if annotation.startswith('http'):
                self.__redis_connection.rpush("video:" + str(video_id) + ":process:" + str(process_id) + ":mediaFragmentsURIS", annotation)
                last_media_fragments_URI = annotation
            else:
                third_line = annotation.split('\n', 3)[2]
                first_property = third_line.split(',')[0]
                value = first_property.split(':', 2)[1]
                identificator = value.strip().replace("\"", "")
                webVTT_annotations.append(annotation)
                if last_media_fragments_URI is not None:
                    self.__redis_connection.rpush("video:" + str(video_id) + ":process:" + str(process_id) + ":object:" + identificator + ":mediaFragmentsURIS", annotation)
                self.__redis_connection.sadd("video:" + str(video_id) + ":process:" + str(process_id) + ":objects", identificator)

        file_name = self.__redis_connection.get("video:" + str(video_id) + ":process:"+str(process_id)+":webVTTFile")
        if file_name is not None:
            file_name_string = "".join(chr(x) for x in bytearray(file_name))
            file_path = os.path.join(settings.ANNOTATIONS_ROOT, file_name_string)
            self.save_webVTT_metadata(file_path, webVTT_annotations)

    def get_videos(self):
        """
        This method gets and returns the list with identificators of all uploaded video from Redis.

        :return: List with identificators of all uploaded video from Redis - ensured by LRANGE from 0 to -1
        """
        videos = self.__redis_connection.lrange("video", 0, -1)
        videos_list = []
        for video in videos:
            video_id = int(video)
            video_detail = self.get_video_info(video_id)
            if video_detail is None:
                continue

            path = video_detail['path']
            metadata = video_detail['metadata']
            video_url = constants.API_URL + "video/" + str(video_id)
            videos_list.append({'id': video_id, 'path': path, 'metadata': metadata, 'URL': video_url})
        return videos_list

    def get_video_info(self, video_id):
        """
        This method gets the information about the video with the given identificator from Redis and returns them in a hash representing the detail of the video. If video with the given identificator does not exists, it returns None value.

        :param video_id: Identificator of the video
        :return: Hash with detailed information about the video (path to the file, metadata and list of processes ran for the video), if the video with the given identificator exists. None, if the video with the given identificator does not exist.
        """
        if not self.__redis_connection.exists("video:" + str(video_id) + ":path"):
            return None
        path = self.__redis_connection.get("video:" + str(video_id) + ":path")
        path_string = constants.MEDIA_URL + "".join(chr(x) for x in bytearray(path))
        metadata = self.__redis_connection.hgetall("video:" + str(video_id) + ":metadata")
        metadata_hash = self.__convert_to_string_keys_and_values(metadata)
        processes = self.__redis_connection.smembers("video:" + str(video_id) + ":processes")
        return {'id': video_id, 'path': path_string, 'metadata': metadata_hash, 'processes': processes}

    def get_processes(self, video_id):
        """
        This method gets the list of identificators of processes ran for the video with the given identificator from Redis set and returns it.

        :param video_id: Identificator of the video
        :return: List of members of set with identificator of processes ran for the video with the given identificator.
        """
        processes = self.__redis_connection.smembers("video:"+str(video_id)+":processes")
        list_processes = []
        for process in processes:
            process_id = int(process)
            process_info = self.get_process_info(video_id, process_id)
            url = constants.API_URL + 'video/' + str(video_id) + '/process/' + str(process_id) + '/'
            list_processes.append({'id': process_id, 'status': process_info['status'], 'URL': url})
        return list_processes

    def get_process_info(self, video_id, process_id):
        """
        This method gets the detailed information about the process with the given identificator which processes the video with the given identificator. It gets the path to WebVTT file, hash with parameters and set of detected objects from Redis and then returns a hash with the detailed information got from Redis. If the process with the given identificator does not exist, or the process was ran for a different video with different identificator, it returns None.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :return: Hash with detailed information (parameters which were given before running the process, path to WebVTT file with metadata and list of detected objects) about the process with the given identificator. None, if the video or the process does not exist or the process did not processed the video with the given identificator.
        """
        if not self.__redis_connection.exists("video:"+str(video_id)+":process:"+str(process_id)+":parameters"):
            return None
        parameters = self.__redis_connection.hgetall("video:"+str(video_id)+":process:"+str(process_id)+":parameters")
        parameters_hash = self.__convert_to_string_keys_and_values(parameters)
        parameters_hash = self.__convert_indexed_properties_to_array(parameters_hash)
        webVTT_file = self.__redis_connection.get("video:"+str(video_id)+":process:"+str(process_id)+":webVTTFile")
        objects = self.__redis_connection.smembers("video:"+str(video_id)+":process:"+str(process_id)+":objects")
        status = self.get_process_status(video_id, process_id)
        media_fragment_URIs = self.__redis_connection.lrange("video:" + str(video_id) + ":process:" + str(process_id) + ":mediaFragmentsURIS", 0, -1)
        media_fragment_URIs_strings = self.__strings_from_byte_arrays_in_array(media_fragment_URIs)
        return {'id': process_id, 'parameters': parameters_hash, 'webVTTFile': webVTT_file, 'objects': objects, 'status': status, 'mediaFragmentsURIs': media_fragment_URIs_strings}

    def get_objects(self, video_id, process_id):
        """
        This function gets the members of the set with identificators of detected objects during the process with the given identificator. It returns a list with the got members of set with identificator of detected objects from Redis.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :return: The list with members of the set with identificators of objects detected during the given process from Redis.
        """
        return self.__redis_connection.smembers("video:"+str(video_id)+":process:"+str(process_id)+":objects")

    def get_object_annotations(self, video_id, process_id, object_id):
        """
        This method gets annotations for the moving object with the given identificator which was detected during the given process processing the given video. It returns a hash with a list of Media Fragment URIs from Redis and list of WebVTT fragments from Redis, if object with the given identificator exists and was detected during the given process. Otherwise it returns None value.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        :param object_id: Identificator of the detected moving object
        :return: Hash with the list of Media Fragments URIs and list of WebVTT fragments for the given object, if the object exists and was detected during the given process processing the given video. If the object with the given identificator does not exist, or it was detected during a different process, it returns None.
        """
        if not self.__redis_connection.exists("video:" + str(video_id) + ":process:" + str(process_id) + ":object:" + str(object_id) + ":mediaFragmentsURIS"):
            return None
        media_fragment_URIs = self.__redis_connection.lrange("video:" + str(video_id) + ":process:" + str(process_id) + ":object:" + str(object_id) + ":mediaFragmentsURIS", 0, -1)
        media_fragment_URIs_strings = self.__strings_from_byte_arrays_in_array(media_fragment_URIs)
        return {'id': object_id, 'mediaFragmentsURIs': media_fragment_URIs_strings}

    def delete_video(self, video_id):
        """
        This method deletes the video with all informations and processes ran for the video from Redis and it also removes WebVTT files with metadata and the video file.

        :param video_id: Identificator of the video
        """
        processes = self.__redis_connection.smembers("video:" + str(video_id) + ":processes")
        for process in processes:
            self.delete_process(video_id, int(process))
        self.__redis_connection.delete("video:" + str(video_id) + ":processes")
        self.__redis_connection.delete("video:" + str(video_id) + ":metadata")
        self.__redis_connection.delete("video:" + str(video_id) + ":author")
        video_path = self.__redis_connection.get("video:" + str(video_id) + ":path")
        video_path_string = "".join(chr(x) for x in bytearray(video_path))
        video_path_string = video_path_string.replace('http://localhost:8000/', '')
        self.__redis_connection.delete("video:" + str(video_id) + ":path")
        os.remove(os.path.join(settings.MEDIA_ROOT, video_path_string))
        self.__redis_connection.lrem("video", -1, video_id)

    def delete_process(self, video_id, process_id):
        """
        This method removes all information about the given process and detected object during the process. It also removes the file with WebVTT metadata generated for the process.

        :param video_id: Identificator of the video
        :param process_id: Identificator of the process
        """
        objects = self.get_objects(video_id, process_id)
        for object in objects:
            object_id = int(object)
            self.__redis_connection.delete("video:" + str(video_id) + ":process:" + str(process_id) + ":object:" + str(object_id) + ":mediaFragmentsURIS")
        self.__redis_connection.delete("video:" + str(video_id) + ":process:" + str(process_id) + ":objects")
        self.__redis_connection.delete("video:" + str(video_id) + ":process:" + str(process_id) + ":parameters")
        self.__redis_connection.delete("video:" + str(video_id) + ":process:" + str(process_id) + ":status")
        self.__redis_connection.delete("video:" + str(video_id) + ":process:" + str(process_id) + ":mediaFragmentsURIS")
        webVTT_file_path = self.__redis_connection.get("video:" + str(video_id) + ":process:" + str(process_id) + ":webVTTFile")

        webVTT_file_path_string = 'metadata' + str(process_id) + '.vtt'
        if webVTT_file_path is not None:
            webVTT_file_path_string = "".join(chr(x) for x in bytearray(webVTT_file_path))

        self.__redis_connection.delete("video:" + str(video_id) + ":process:" + str(process_id) + ":webVTTFile")
        webVTT_file_path = os.path.join(settings.ANNOTATIONS_ROOT, webVTT_file_path_string)
        if os.path.exists(webVTT_file_path):
            os.remove(webVTT_file_path)
        self.__redis_connection.srem("video:" + str(video_id) + ":processes", process_id)
        self.__redis_connection.lrem("process", -1, process_id)

    def get_video_max_id(self):
        """
        :return: The maximal identificator of a saved video which is saved in Redis
        """
        max_video_id = 0
        videos = self.__redis_connection.lrange("video", -1, -1)
        for video in videos:
            max_video_id = int(video)
        return max_video_id

    def get_process_max_id(self):
        """"
        :return: The maximal identificator of a saved and ran process from Redis
        """
        max_process_id = 0
        processes = self.__redis_connection.lrange("process", -1, -1)
        for process in processes:
            max_process_id = int(process)
        return max_process_id

    def get_process_status(self, video_id, process_id):
        """
        This method gets and returns the status of the process with the given identificator. It returns the status of the process, if it is saved. If the status of the process is not saved, it returns "Not started" string representing that the process has not been started and the status of the process is not saved.

        :param video_id: Identificator of the video processed by the process
        :param process_id: Identificator of the process
        :return: If the status is not saved in Redis, it returns "Not started" string representing that the process has not been started yet.
        """
        status = self.__redis_connection.get("video:" + str(video_id) + ":process:" + str(process_id) + ":status")
        if status is None:
            return "Not started"
        else:
            return "".join(chr(x) for x in bytearray(status))

    def set_process_status(self, video_id, process_id, status):
        """
        This method saves the current status of the process with the given identificator to Redis.

        :param video_id: Identificator of the video processed by the process
        :param process_id: Identificator of the process
        :param status: Current status of the process
        """
        self.__redis_connection.set("video:" + str(video_id) + ":process:" + str(process_id) + ":status", status)

    def __convert_to_string_keys_and_values(self, byte_hash):
        """
        This method converts a hash where keys and values are byte arrays to a hash, where keys and values are strings.

        :param byte_hash: Hash where keys and values are byte arrays
        :return: Hash where keys and values are not byte arrays, but strings
        """
        string_hash = {}
        for key in byte_hash:
            new_key = "".join(chr(x) for x in bytearray(key))
            new_value = "".join(chr(x) for x in bytearray(byte_hash[key]))
            string_hash[new_key] = new_value
        return string_hash

    def __convert_indexed_properties_to_array(self, hash):
        """
        This method converts a hash with indexed properties to a hash with array under a key without a number index.

        :param hash: Hash got from Redis with keys containing a number index and key under which is saved an array.
        :return: Hash without keys containing a number index and key under which is saved an array which contains keys without indexes and arrays as values.
        """
        new_hash = {}
        for key in hash:
            if ':' in key:
                array_and_index = key.split(':', 2)
                array = array_and_index[0]
                index = int(array_and_index[1])
                if index == 0:
                    new_hash[array] = [hash[key]]
                else:
                    new_hash[array].append(hash[key])
            else:
                new_hash[key] = hash[key]
        return new_hash

    def __strings_from_byte_arrays_in_array(self, byte_array_array):
        """
        This method converts elements from byte arrays to strings in the array and it returns the result array with strings.

        :param byte_array_array: An array where elements are bytearrays.
        :return: Array where elements are strings, not bytearrays.
        """
        string_array = []
        for byte_array_element in byte_array_array:
            string_element = "".join(chr(x) for x in bytearray(byte_array_element))
            string_array.append(string_element)
        return string_array