import base64
from django.db import models


def get_token_from_username_and_password(username, password):
    """
    This function creates a Base64 token from the given username and password for HTTP basic authentication to REST API.

    :param username: Username
    :param password: Password
    :return: Base64 token created from the given username and password.
    """
    user_credentials = (username + ':' + password).encode('ascii')
    base64_token_bytes = base64.b64encode(user_credentials)
    return base64_token_bytes.decode('ascii')