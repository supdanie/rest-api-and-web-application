from django.apps import AppConfig


class QuickstartConfig(AppConfig):
    name = 'video_processing_rest_api'
