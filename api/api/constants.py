SERVER_URL = 'http://localhost:8000/'
"""
It is the URL of the current server.
"""

API_URL = 'http://localhost:8000/api/'
"""
It is the URL of the REST API on the current server.
"""

MEDIA_URL = 'http://localhost:8000/media/'
"""
It is the URL of the media directory with videos on the current server.
"""
STATUS_RUNNING = 'Running'
"""
It represents that a process is running.
"""
STATUS_DONE = 'Done'
"""
It represents that a process is finished.
"""