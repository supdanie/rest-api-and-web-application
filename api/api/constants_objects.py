from api.video_processing_rest_api.storage import RedisStorage

STORAGE = RedisStorage()
"""
The object representing the used storage for saving information about video, processes and detected moving objects.
"""