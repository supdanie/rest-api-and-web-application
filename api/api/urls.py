"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from rest_framework import routers
from rest_framework import permissions
from api.video_processing_rest_api import views
from django.urls import include, path
from rest_framework_nested import routers
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
import api.pages.views as pages
from django.conf.urls.static import static
import api.settings as settings

router = routers.SimpleRouter()
router.register('video', views.VideoViewSet, basename='video')

process_router = routers.NestedSimpleRouter(router, 'video', lookup='video')
process_router.register('process', views.ProcessViewSet, basename='video_process')

object_router = routers.NestedSimpleRouter(process_router, 'process', lookup='process')
object_router.register('object', views.ObjectViewSet, basename='video_process_object')

schema_view = get_schema_view(
    openapi.Info(
        title='REST API for processing video',
        default_version='v1',
        description='Todo'
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('login', pages.login_view, name='login'),
    path('', pages.home_view, name='home'),
    path('video/<pk>/add_process', pages.add_process_view, name='add_process'),
    path('video/<pk>/process', pages.processes_view, name='processes'),
    path('video/<video_pk>/process/<pk>', pages.process_detail_view, name='process_detail'),
    path('api/', include(router.urls)),
    path('api/', include(process_router.urls)),
    path('api/', include(object_router.urls)),
    path('swagger.json',  schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('api/api/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.ANNOTATIONS_URL, document_root=settings.ANNOTATIONS_ROOT)
