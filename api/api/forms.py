from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.validators import FileExtensionValidator

from django import forms

class VideoForm(forms.Form):
    """
    This class represents a form for uploading a video to server.
    """
    video = forms.FileField(label='Upload video', validators=[FileExtensionValidator(allowed_extensions=['mp4', 'webm', 'ogv'])], help_text='Upload a video file with extension mp4, webm or ogv.')
    """
    Field for uploading a video file.
    """

class LoginForm(forms.Form):
    """
    This class represents the login form for signing an user.
    """
    username = forms.CharField(label='Username')
    """
    Text field for filling the username
    """
    password = forms.CharField(label='Password', widget=forms.PasswordInput())
    """
    Text field for filling the password
    """

class ProcessForm(forms.Form):
    """
    This class represents a form for starting a new process for processing video, object detection, speed measurement and annotation generation.
    """
    h = forms.IntegerField(label='Height of the camera under the surface (or road) (in mm)', min_value=1, help_text='Fill the height of the camera center above the surface (for example road with cars) in millimeters.')
    """
    Integer field for filling the height of the camera under the surface (or road).
    """
    alpha = forms.FloatField(label='Pitch angle (in degrees)', widget=forms.NumberInput(attrs={'id': 'alpha', 'step': '0.01'}), help_text='Fill the pitch angle of the camera to the surface.')
    """
    Float field for filling the pitch angle of the camera in degrees.
    """
    f = forms.FloatField(label='Focal distance (in mm)', min_value=0.01, widget=forms.NumberInput(attrs={'id': 'f', 'step': '0.01'}), help_text='Fill the used focal distance (distance between the len and the focus) in millimeters.')
    """
    Float field for filling the focal distance.
    """
    len_width = forms.FloatField(label='Sensor width (in mm)', min_value=0.01, widget=forms.NumberInput(attrs={'id': 'len_width', 'step': '0.01'}), help_text='Fill the width of the sensor in millimeters. For example, for standard 16 mm Film frame you fill "12,7" to this field.')
    """
    Float field for filling the width of the sensor in millimeters.
    """
    len_height = forms.FloatField(label='Sensor height (in mm)', min_value=0.01, widget=forms.NumberInput(attrs={'id': 'len_height', 'step': '0.01'}), help_text='Fill the height of the sensor in millimeters. For example, for standard 16 mm Film frame you fill "7,49" to this field.')
    """
    Float field for filling the height of the sensor in millimeters.
    """
    width = forms.IntegerField(label='Frame width (in px)', min_value=1, help_text='Fill the width of frames in the video in pixels.')
    """
    Integer field for filling the width of the frame in pixels.
    """
    height = forms.IntegerField(label='Frame height (in px)', min_value=1, help_text='Fill the height of frames in the video in pixels.')
    """
    Integer field for filling the height of the frame in pixels.
    """
    p_x = forms.IntegerField(label='x coord of principal point (in px)', min_value=1, help_text='Principal point has x, y coordinates. Fill the x coordinate to this field.')
    """
    Integer field for filling the x coord of the principal point in pixels.
    """
    p_y = forms.IntegerField(label='y coord of principal point (in px)', min_value=1, help_text='Principal point has x, y coordinates. Fill the y coordinate to this field.')
    """
    Integer field for filling the y coord of the principal point in pixels.
    """